Summary:        Line wrapping module
Name:           perl-Text-WrapI18N
Version:        0.06
Release:        34
License:        GPL-1.0-or-later OR Artistic-1.0-Perl

URL:            https://metacpan.org/release/Text-WrapI18N
Source0:        https://cpan.metacpan.org/authors/id/K/KU/KUBOTA/Text-WrapI18N-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  perl-generators perl(Exporter) perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Test::Simple) perl(Text::CharWidth) >= 0.02 perl(strict)
BuildRequires:  perl(vars) perl(warnings)

%{?perl_default_filter}

%description
Line wrapping module with support for multibyte, fullwidth,and
combining characters and languages without whitespaces between words

%package_help

%prep
%autosetup -p1 -n Text-WrapI18N-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%{perl_vendorlib}/Text

%files help
%doc README Changes
%{_mandir}/man3/Text::WrapI18N.3pm*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.06-34
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Dec 3 2019 mengxian <mengxian@huawei.com> - 0.06-33
- Package init
